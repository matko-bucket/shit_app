package com.example.kolkosamseizasraoejp.ui.home;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import com.example.kolkosamseizasraoejp.BasicApp;
import com.example.kolkosamseizasraoejp.BuildConfig;
import com.example.kolkosamseizasraoejp.MainActivity;
import com.example.kolkosamseizasraoejp.R;
import com.example.kolkosamseizasraoejp.helper.LocaleHelper;
import com.example.kolkosamseizasraoejp.ui.DataRepository;
import org.joda.time.LocalDate;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private MainActivity main;
    private DataRepository dataRepository;
    private Button addShitButton;
    private Button minusShitButton;
    private ImageView happyShit;
    private ImageView sadShit;
    private int totalShits;
    private TextView build;

    @RequiresApi(api = Build.VERSION_CODES.O)
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        main = (MainActivity) getActivity();

//        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        dataRepository = ((BasicApp) getActivity().getApplication()).getRepository();
        homeViewModel = new HomeViewModel(dataRepository);

        final TextView textView = root.findViewById(R.id.text_home);
        homeViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });

        TextView date = root.findViewById(R.id.date);
        String dateString = LocaleHelper.formatJavaTimeLocalDateDDMMYYYY(LocalDate.now().toString());
        date.setText(dateString);

        addShitButton = root.findViewById(R.id.shit_add);
        minusShitButton = root.findViewById(R.id.shit_minus);
        happyShit = root.findViewById(R.id.happy_shit);
        sadShit = root.findViewById(R.id.sad_shit);
        build = root.findViewById(R.id.build);

        build.setText("v"+BuildConfig.VERSION_NAME);

        homeViewModel.getTotalShit().observe(getViewLifecycleOwner(), totalShits ->{
            this.totalShits = totalShits;
        });

        addShitButton.setOnClickListener(view -> {
            homeViewModel.addShit();
            addShitButton.setVisibility(view.INVISIBLE);
            happyShit.setVisibility(view.VISIBLE);
            Toast.makeText(getContext(), getString(R.string.shit_success), Toast.LENGTH_SHORT).show();
            main.PlayBackgroundSound(view);
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    addShitButton.setVisibility(view.VISIBLE);
                    happyShit.setVisibility(view.GONE);
                }
            }, 3500);
        });

        minusShitButton.setOnClickListener(view -> {
            if(totalShits == 0){
                Toast.makeText(getContext(), getString(R.string.no_shit), Toast.LENGTH_SHORT).show();
            } else {
                homeViewModel.minusShit();
                minusShitButton.setVisibility(view.INVISIBLE);
                sadShit.setVisibility(view.VISIBLE);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        minusShitButton.setVisibility(view.VISIBLE);
                        sadShit.setVisibility(view.GONE);
                    }
                }, 1500);
            }
        });



        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}