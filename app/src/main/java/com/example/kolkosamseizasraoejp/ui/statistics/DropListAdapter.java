package com.example.kolkosamseizasraoejp.ui.statistics;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

import com.example.kolkosamseizasraoejp.R;
import com.example.kolkosamseizasraoejp.database.entity.DropEntity;

public class DropListAdapter extends ArrayAdapter<DropEntity> {

    private Context mContext;
    int mResource;

    public DropListAdapter(@NonNull Context context, int resource, @NonNull ArrayList<DropEntity> objects) {
        super(context, resource, objects);
        mContext = context;
        mResource = resource;
    }

    @SuppressLint("ViewHolder")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        String time = getItem(position).getTime().substring(0, 5);

        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource, parent, false);
        TextView timeTextView = convertView.findViewById(R.id.time1);
        timeTextView.setText(time);

        return convertView;
    }
}
