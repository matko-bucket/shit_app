package com.example.kolkosamseizasraoejp.ui.statistics;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.kolkosamseizasraoejp.database.entity.DropEntity;
import com.example.kolkosamseizasraoejp.ui.DataRepository;

import java.util.ArrayList;
import java.util.List;

public class StatisticsViewModel extends ViewModel {

    private DataRepository dataRepository;
    private MutableLiveData<Integer> totalNumberOfShits = new MutableLiveData<>();
    private MutableLiveData<String> zeroDate = new MutableLiveData<>();
    private MutableLiveData<List<DropEntity>> dropEntities = new MutableLiveData<>();
    private LiveData<String> startDate = new MutableLiveData<>();


    public StatisticsViewModel(DataRepository dataRepository) {
        this.dataRepository = dataRepository;
        totalNumberOfShits.setValue(dataRepository.getTotalShitNumber());
        zeroDate.setValue(dataRepository.getZeroDate());
        startDate = dataRepository.getStartDate();
    }

    public LiveData<Integer> getTotalShit(){
        return totalNumberOfShits;
    }

    public LiveData<String> getZeroDate(){
        return zeroDate;
    }

    public LiveData<List<DropEntity>> getDropsByDateRange(String start, String end){
        return dataRepository.loadDropsByDate(start, end);
    }

    public LiveData<String> getStartDate(){
        return startDate;
    }

    public LiveData<List<DropEntity>> getDropsBySingleDate(String date){
        return dataRepository.loadDropsBySingleDate(date);
    }
}