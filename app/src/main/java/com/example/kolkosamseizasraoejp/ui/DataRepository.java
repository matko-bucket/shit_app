package com.example.kolkosamseizasraoejp.ui;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.lifecycle.LiveData;

import com.example.kolkosamseizasraoejp.database.AppDatabase;
import com.example.kolkosamseizasraoejp.database.entity.DropEntity;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import java.util.ArrayList;
import java.util.List;

public class DataRepository {

    private static volatile DataRepository sInstance;
    private Context context;
    private final AppDatabase mDatabase;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private String SHARED_PREF_NAME = "session";
    private String TOTAL_NUMBER_OF_SHIT = "total_number_of_shit";
    private String NUMBER_OF_DAYS = "number_of_days";
    private String ZERO_DATE = "zero_date";


    private String SESSION_REMEMBER_ME_KEY = "session_remember_me";
    private String SESSION_FIRST_NAME_KEY = "session_first_name";
    private String SESSION_LAST_NAME_KEY = "session_last_name";
    private String SESSION_EMAIL_KEY = "session_email";


    public DataRepository(Context context, final AppDatabase database){
        mDatabase = database;
        this.sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        this.editor = sharedPreferences.edit();
        this.context = context;
        if(getZeroDate().equals("2000-01-01")){
            LocalDate zeroDate = LocalDate.now();
            String zeroDateString = zeroDate.toString();
            editor.putString(ZERO_DATE, zeroDateString).commit();
        }

        editor.putString(ZERO_DATE, getZeroDate());
        sInstance = this;
    }

    public static DataRepository getInstance(Context context, final AppDatabase database){
        if (sInstance == null) {
            synchronized (DataRepository.class) {
                if (sInstance == null) {
                    sInstance = new DataRepository(context, database);
                }
            }
        }
        return sInstance;
    }

    public void addShit(){
        editor.putInt(TOTAL_NUMBER_OF_SHIT, getTotalShitNumber() + 1).commit();
        DropEntity drop = new DropEntity();
        drop.setDate(LocalDate.now().toString());
        drop.setTime(LocalTime.now().toString());
        mDatabase.insertDrop(drop);
    }

    public void minusShit(){
        editor.putInt(TOTAL_NUMBER_OF_SHIT, getTotalShitNumber() - 1).commit();
        mDatabase.deleteDrop();
    }

    public int getTotalShitNumber(){
        return sharedPreferences.getInt(TOTAL_NUMBER_OF_SHIT, 0);
    }

    public String getZeroDate(){
        return sharedPreferences.getString(ZERO_DATE, "2000-01-01");
    }

    public LiveData<List<DropEntity>> loadDropsByDate(String start, String end) {
        return mDatabase.dropDao().loadDropsByDateRange(start, end);
    }
    public LiveData<List<DropEntity>> loadDropsBySingleDate(String date) {
        return mDatabase.dropDao().loadDropsByDate(date);
    }
    public LiveData<String> getStartDate(){
        return mDatabase.dropDao().getStartDate();
    }
}
