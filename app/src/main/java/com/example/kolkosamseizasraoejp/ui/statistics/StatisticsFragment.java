package com.example.kolkosamseizasraoejp.ui.statistics;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.kolkosamseizasraoejp.BasicApp;
import com.example.kolkosamseizasraoejp.R;
import com.example.kolkosamseizasraoejp.database.entity.DropEntity;
import com.example.kolkosamseizasraoejp.databinding.FragmentNotificationsBinding;
import com.example.kolkosamseizasraoejp.helper.LocaleHelper;
import com.example.kolkosamseizasraoejp.ui.DataRepository;

import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.Calendar;

public class StatisticsFragment extends Fragment {

    private StatisticsViewModel statisticsViewModel;
    private FragmentNotificationsBinding binding;
    private TextView numberOfDays;
    private TextView numberOfShit;
    private DataRepository dataRepository;
    private TextView shitAverage;
    private int totalShit;
    private int totalDays;
    final Calendar myCalendarFrom = Calendar.getInstance();
    final Calendar myCalendarTo = Calendar.getInstance();
    private EditText dropFrom;
    private EditText dropTo;
    private String dateFromDb;
    private String dateToDb;
    private Button findByDateRangeButton;
    private TextView totalShitByDateRange;
    private TextView byRangeText;
    private TextView daysByRangeText;
    private TextView shitAverageByRange;
    private TextView fromBeginningOfTimeText;
    private EditText singleDrop;
    final Calendar myCalendarSingle = Calendar.getInstance();
    private String dateSingleDb;
    private Button findBySingleDateButton;
    private ListView mListViewDropsByDate;
    private DropListAdapter dropAdapter;
    private TextView noDropsText;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        dataRepository = ((BasicApp) getActivity().getApplication()).getRepository();
        statisticsViewModel = new StatisticsViewModel(dataRepository);

        binding = FragmentNotificationsBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        numberOfDays = root.findViewById(R.id.number_of_days);
        numberOfShit = root.findViewById(R.id.number_of_shit);
        shitAverage = root.findViewById(R.id.shit_average);
        dropFrom = binding.dropFrom;
        dropTo = binding.dropTo;
        findByDateRangeButton = binding.findByDateRange;
        totalShitByDateRange = binding.numberOfShitByRange;
        byRangeText = binding.byRangeText;
        daysByRangeText = binding.numberOfDaysByRange;
        shitAverageByRange = binding.shitAverageByRange;
        fromBeginningOfTimeText = binding.fromBegginingOfTime;
        singleDrop = binding.singleDate;
        findBySingleDateButton = binding.singleDateButton;
        mListViewDropsByDate = binding.dropsPerDayListView;
        noDropsText = binding.noDropsText;

        statisticsViewModel.getTotalShit().observe(getViewLifecycleOwner(), totalShits ->{
            numberOfShit.setText("Ukupno dropova: "+totalShits);
            this.totalShit = totalShits;
        });

        statisticsViewModel.getZeroDate().observe(getViewLifecycleOwner(), zeroDate ->{
            DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
            LocalDate currentDate = LocalDate.now();
            LocalDate startDate = formatter.parseLocalDate(zeroDate);
//            DateTime testDate = new DateTime(2022, 01, 06, 5, 0, 0);
            int daysTotal = Days.daysBetween(startDate, currentDate).getDays();
            numberOfDays.setText("Ukupno dana: " + daysTotal);
            this.totalDays = daysTotal;
            shitAverage.setText("Prosečno dropova po danu: " + LocaleHelper.average(totalShit, totalDays));
            String zeroDateString = LocaleHelper.formatJavaTimeLocalDateDDMMYYYY(zeroDate);
            fromBeginningOfTimeText.setText("Svi dropovi ot vremena ("+zeroDateString+")");
        });

        statisticsViewModel.getStartDate().observe(getViewLifecycleOwner(), startDate -> {
            if(startDate == null){
                java.time.LocalDate localDate = java.time.LocalDate.now();//For reference
                startDate = LocaleHelper.formatJavaTimeLocalDateDDMMYYYY(localDate);
            } else {
                startDate = LocaleHelper.formatJavaTimeLocalDateDDMMYYYY(startDate);
            }
            byRangeText.setText("Dropovi sa detumom kreću od: "+startDate);
        });

        DatePickerDialog.OnDateSetListener dateFrom =new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                myCalendarFrom.set(Calendar.YEAR, year);
                myCalendarFrom.set(Calendar.MONTH,month);
                myCalendarFrom.set(Calendar.DAY_OF_MONTH,day);
                updateLabelFrom();
            }
        };
        DatePickerDialog.OnDateSetListener dateTo =new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                myCalendarTo.set(Calendar.YEAR, year);
                myCalendarTo.set(Calendar.MONTH,month);
                myCalendarTo.set(Calendar.DAY_OF_MONTH,day);
                updateLabelTo();
            }
        };
        DatePickerDialog.OnDateSetListener singleDate =new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                myCalendarSingle.set(Calendar.YEAR, year);
                myCalendarSingle.set(Calendar.MONTH,month);
                myCalendarSingle.set(Calendar.DAY_OF_MONTH,day);
                updateLabelSingle();
            }
        };
        dropFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(getContext(),dateFrom, myCalendarFrom.get(Calendar.YEAR), myCalendarFrom.get(Calendar.MONTH), myCalendarFrom.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        dropTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(getContext(),dateTo, myCalendarTo.get(Calendar.YEAR), myCalendarTo.get(Calendar.MONTH), myCalendarTo.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        singleDrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(getContext(), singleDate, myCalendarSingle.get(Calendar.YEAR), myCalendarSingle.get(Calendar.MONTH), myCalendarSingle.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        findByDateRangeButton.setOnClickListener(v->{
            if(dateFromDb == null || dateToDb == null){
                totalShitByDateRange.setText("Unesi oba datuma seronjo");
            } else {
                statisticsViewModel.getDropsByDateRange(dateFromDb, dateToDb).observe(getViewLifecycleOwner(), dropList ->{
                    totalShitByDateRange.setText("Ukupno dropova: " + dropList.size());
                    int numberOfDays = LocaleHelper.daysBetween(dateFromDb, dateToDb) + 1;
                    daysByRangeText.setText("Ukupno dana: "+numberOfDays);
                    shitAverageByRange.setText("Prosečno dropova: " + LocaleHelper.average(dropList.size(), numberOfDays));
                });
            }
        });

        findBySingleDateButton.setOnClickListener(v->{
            if(dateSingleDb == null){
                Toast.makeText(getContext(), "A di ga detum, m?", Toast.LENGTH_SHORT).show();
            } else {
                statisticsViewModel.getDropsBySingleDate(dateSingleDb).observe(getViewLifecycleOwner(), dropList ->{
                    if(dropList.size() == 0){
                        noDropsText.setVisibility(View.VISIBLE);
                    } else {
                        noDropsText.setVisibility(View.GONE);
                        dropAdapter = new DropListAdapter(getContext(), R.layout.adapter_view_drops_by_date, (ArrayList<DropEntity>) dropList);
                        mListViewDropsByDate.setAdapter(dropAdapter);
                    }
                });
            }
        });

        return root;
    }


    private void updateLabelFrom(){
        dropFrom.setText(LocaleHelper.formatDateDDMMYYYY(myCalendarFrom.getTime()));
        dateFromDb = LocaleHelper.formatDateYYYYMMDD(myCalendarFrom.getTime());
    }
    private void updateLabelTo(){
        dropTo.setText(LocaleHelper.formatDateDDMMYYYY(myCalendarTo.getTime()));
        dateToDb = LocaleHelper.formatDateYYYYMMDD(myCalendarTo.getTime());
    }
    private void updateLabelSingle(){
        singleDrop.setText(LocaleHelper.formatDateDDMMYYYY(myCalendarSingle.getTime()));
        dateSingleDb = LocaleHelper.formatDateYYYYMMDD(myCalendarSingle.getTime());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}