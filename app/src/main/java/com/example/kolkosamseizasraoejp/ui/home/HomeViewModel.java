package com.example.kolkosamseizasraoejp.ui.home;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.kolkosamseizasraoejp.ui.DataRepository;

public class HomeViewModel extends ViewModel {

    private MutableLiveData<String> mText;
    private DataRepository dataRepository;
    private MutableLiveData<Integer> totalNumberOfShits = new MutableLiveData<>();

    public HomeViewModel(DataRepository dataRepository) {
        this.dataRepository = dataRepository;
        mText = new MutableLiveData<>();
        mText.setValue("sranje aplikejcija");
        totalNumberOfShits.setValue(dataRepository.getTotalShitNumber());
    }

    public LiveData<String> getText() {
        return mText;
    }


    public void addShit(){
        totalNumberOfShits.setValue(dataRepository.getTotalShitNumber()+1);
        dataRepository.addShit();
    }

    public LiveData<Integer> getTotalShit(){
        return totalNumberOfShits;
    }

    public void minusShit(){
        totalNumberOfShits.setValue(dataRepository.getTotalShitNumber()-1);
        dataRepository.minusShit();
    }
}