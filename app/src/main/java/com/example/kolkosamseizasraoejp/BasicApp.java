package com.example.kolkosamseizasraoejp;

import android.app.Application;

import com.example.kolkosamseizasraoejp.database.AppDatabase;
import com.example.kolkosamseizasraoejp.ui.DataRepository;


/**
 * Android Application class. Used for accessing singletons.
 */
public class BasicApp extends Application {

    private com.example.kolkosamseizasraoejp.AppExecutors mAppExecutors;

    @Override
    public void onCreate() {
        super.onCreate();
        mAppExecutors = new com.example.kolkosamseizasraoejp.AppExecutors();
    }

    public AppDatabase getDatabase() {
        return AppDatabase.getInstance(this, mAppExecutors);
    }

    public DataRepository getRepository() {
        return DataRepository.getInstance(getApplicationContext(), getDatabase());
    }

}

