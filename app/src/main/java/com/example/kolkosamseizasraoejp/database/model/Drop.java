package com.example.kolkosamseizasraoejp.database.model;

public interface Drop {
    public int getId();
    public String getDate();
    public String getTime();
}
