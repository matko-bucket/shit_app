package com.example.kolkosamseizasraoejp.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import com.example.kolkosamseizasraoejp.database.entity.DropEntity;

@Dao
public interface DropDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertDrop(DropEntity dropEntity);

    @Query("SELECT * FROM dropovi")
    LiveData<List<DropEntity>> loadAllDrops();

    @Query("SELECT * FROM dropovi WHERE date BETWEEN :start AND :end")
    LiveData<List<DropEntity>> loadDropsByDateRange(String start, String end);

    @Query("SELECT * FROM dropovi WHERE date = :date")
    LiveData<List<DropEntity>> loadDropsByDate(String date);

    @Query("DELETE FROM dropovi WHERE id = (SELECT MAX(id) FROM dropovi)")
    void deleteDrop();

    @Query("SELECT MIN(date) FROM dropovi LIMIT 1")
    LiveData<String> getStartDate();
}
