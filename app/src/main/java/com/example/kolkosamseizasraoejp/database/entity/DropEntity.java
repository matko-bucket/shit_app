package com.example.kolkosamseizasraoejp.database.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.kolkosamseizasraoejp.database.model.Drop;


@Entity(tableName = "dropovi")
public class DropEntity implements Drop {

    @PrimaryKey(autoGenerate = true)
    protected  int id;
    protected String date;
    protected  String time;

    public DropEntity(){

    }
//    @Ignore
//    public FavoritesEntity(int id,int station_id, String name, int favorite){
//        this.id = id;
//        this.stationId = station_id;
//        this.stationName = name;
//        this.favorite = favorite;
//    }

    @Override
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String getTime() {
        return time;
    }
    public void setTime(String time) {
        this.time = time;
    }

}
