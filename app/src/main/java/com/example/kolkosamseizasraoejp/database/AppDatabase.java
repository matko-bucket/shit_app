package com.example.kolkosamseizasraoejp.database;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.kolkosamseizasraoejp.AppExecutors;
import com.example.kolkosamseizasraoejp.database.dao.DropDao;
import com.example.kolkosamseizasraoejp.database.entity.DropEntity;

@Database(entities = {DropEntity.class}, version = 1,exportSchema = false)

public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase sInstance;

    @VisibleForTesting
    public static final String DATABASE_NAME = "drop";

    public abstract DropDao dropDao();

    private final MutableLiveData<Boolean> mIsDatabaseCreated = new MutableLiveData<>();

    public static AppDatabase getInstance(final Context context, final AppExecutors executors) {
        if (sInstance == null) {
            synchronized (AppDatabase.class) {
                if (sInstance == null) {
                    sInstance = buildDatabase(context.getApplicationContext(), executors);
                    sInstance.updateDatabaseCreated(context.getApplicationContext());
                }
            }
        }
        return sInstance;
    }

    /**
     * Build the database. {@link Builder#build()} only sets up the database configuration and
     * creates a new instance of the database.
     * The SQLite database is only created when it's accessed for the first time.
     */
    private static AppDatabase buildDatabase(final Context appContext, final AppExecutors executors) {
        return Room.databaseBuilder(appContext, AppDatabase.class, DATABASE_NAME)
                .addCallback(new Callback() {
                    @Override
                    public void onCreate(@NonNull SupportSQLiteDatabase db) {
                        super.onCreate(db);
                        executors.diskIO().execute(() -> {
                            // Add a delay to simulate a long-running operation
//                            addDelay();
                            // Generate the data for pre-population
                            AppDatabase database = AppDatabase.getInstance(appContext, executors);

                            // notify that the database was created and it's ready to be used
                            database.setDatabaseCreated();
                        });
                    }
                })
//                .addMigrations(MIGRATION_1_2)
                .allowMainThreadQueries()
                .build();
    }

    /**
     * Check whether the database already exists and expose it via {@link #getDatabaseCreated()}
     */
    private void updateDatabaseCreated(final Context context) {
        if (context.getDatabasePath(DATABASE_NAME).exists()) {
            setDatabaseCreated();
        }
    }

    private void setDatabaseCreated(){
        mIsDatabaseCreated.postValue(true);
    }


    public void insertDrop(DropEntity dropEntity) {
        sInstance.runInTransaction(() -> {
            sInstance.dropDao().insertDrop(dropEntity);
        });
    }

    public void deleteDrop(){
        sInstance.runInTransaction(() -> {
            sInstance.dropDao().deleteDrop();
        });
    }

    public LiveData<Boolean> getDatabaseCreated() {
        return mIsDatabaseCreated;
    }

    private static final Migration MIGRATION_1_2 = new Migration(1, 2) {

        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
//            database.execSQL("CREATE VIRTUAL TABLE IF NOT EXISTS `stationsFts` USING FTS4("
//                    + "`stationId` INTEGER, `stationName` TEXT, content=`stations`)");
//            database.execSQL("INSERT INTO stationsFts (`rowid`, `stationId`, `stationName`) "
//                    + "SELECT `id`, `stationId`, `stationName` FROM stations");

//            database.execSQL("CREATE TABLE IF NOT EXISTS `favourites` (`id` INTEGER, `stationId`, `stationName` TEXT, `favorite` INTEGER, PRIMARY KEY(`id`))");
//            database.execSQL("INSERT INTO favourites (`id`, `stationId`, `stationName`, `favorite`) "
//                    + "SELECT `id`, `stationId`, `stationName`, `favourite` FROM stations");
//
//            database.execSQL("CREATE VIRTUAL TABLE IF NOT EXISTS `favorites` USING FTS4("
//                    + "`stationId` INTEGER, `stationName` TEXT, `favorite` INTEGER, content=`stations`)");
//            database.execSQL("INSERT INTO favorites (`rowid`, `stationId`, `stationName`, `favorite`) "
//                    + "SELECT `id`, `stationId`, `stationName`, `favourite` FROM stations");

//            database.execSQL("ALTER TABLE stations ADD COLUMN `favourite` INTEGER NOT NULL DEFAULT 0");
//            database.execSQL("CREATE VIRTUAL TABLE IF NOT EXISTS `user_info` USING FTS4("
//                    + "`id` INTEGER, `login` TEXT, `firstName` TEXT, `lastName` TEXT, `jmbg` TEXT," +
//                    " `address` TEXT, `email` TEXT, `idCardSn` TEXT, `groupId` INTEGER," +
//                    "`crmContactsCustomerId` TEXT, `monthlyCardUserSn` TEXT, `prepaidCardUserSn` TEXT," +
//                    "`haveCrm` INTEGER, `jmbgMust` INTEGER, `validJmbg` TEXT, `valueJmbg` TEXT, PRIMARY KEY(`id`))");
//            database.execSQL("CREATE TABLE IF NOT EXISTS `user_info` (`id` INTEGER, `login` TEXT, `firstName` TEXT," +
//                    " `lastName` TEXT, `jmbg` TEXT, `address` TEXT, `email` TEXT, `idCardSn` TEXT, `groupId` INTEGER NOT NULL," +
//                    " `crmContactsCustomerId` TEXT, `monthlyCardUserSn` TEXT, `prepaidCardUserSn` TEXT, `haveCrm` INTEGER NOT NULL," +
//                    " `jmbgMust` INTEGER NOT NULL, `validJmbg` TEXT, `valueJmbg` TEXT, PRIMARY KEY(`id`))");
//            database.execSQL("CREATE TABLE IF NOT EXISTS `monthly_card` (`id` INTEGER, `customerFirstName` TEXT, `customerLastName` TEXT," +
//                    " `templateName` TEXT, `cardValidFrom` TEXT, `cardValidTo` TEXT, `cardUserSn` INTEGER NOT NULL, `rideDateFrom` TEXT," +
//                    " `rideDateTo` TEXT, `cardSN` INTEGER, `status` TEXT, `blackListStatus` INTEGER NOT NULL, `mifareSn` TEXT," +
//                    " `cardNo` INTEGER NOT NULL, `validButton` INTEGER, `canProlongDateFrom` TEXT, `canProlongDateTo` TEXT," +
//                    " `dateNewValidityFrom` TEXT, `dateNewValidityTo` TEXT, PRIMARY KEY(`id`))");
//            database.execSQL("CREATE TABLE IF NOT EXISTS `e_wallet` (`id` INTEGER, `customerFirstName` TEXT, `customerLastName` TEXT," +
//                    " `templateName` TEXT, `cardValidFrom` TEXT, `cardValidTo` TEXT, `cardUserSn` INTEGER NOT NULL, `rideDateFrom` TEXT," +
//                    " `rideDateTo` TEXT, `cardSN` INTEGER, `status` TEXT, `blackListStatus` INTEGER NOT NULL, `mifareSn` TEXT," +
//                    " `cardNo` INTEGER NOT NULL, `validButton` INTEGER, PRIMARY KEY(`id`))");
//            database.execSQL("CREATE TABLE IF NOT EXISTS `e_wallet_balance` (`id` INTEGER NOT NULL, `balance` INTEGER NOT NULL, PRIMARY KEY(`id`))");
//            database.execSQL("CREATE TABLE IF NOT EXISTS `card_types` (`id` INTEGER NOT NULL, `description` TEXT, `cardType` INTEGER NOT NULL, " +
//                    "`validityPeriod` INTEGER NOT NULL, `changedBy` TEXT, `dateTime` TEXT, `validityFixedDateTo` TEXT," +
//                    " `cardSerialFrom` INTEGER, `cardSerialTo` INTEGER, PRIMARY KEY(`id`))");

        }
    };
}
