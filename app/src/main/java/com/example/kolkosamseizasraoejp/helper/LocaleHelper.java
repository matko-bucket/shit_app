package com.example.kolkosamseizasraoejp.helper;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ScrollView;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.example.kolkosamseizasraoejp.R;

import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * This class is used to change your application locale and persist this change for the next time
 * that your app is going to be used.
 * <p/>
 * You can also change the locale of your application on the fly by using the setLocale method.
 */
public class LocaleHelper {

    private static final String SELECTED_LANGUAGE = "com.example.testjedan.language";

    public static Context onAttach(Context context) {
        String lang = getPersistedData(context, Locale.getDefault().getLanguage());
        return setLocale(context, lang);
    }

    public static Context onAttach(Context context, String defaultLanguage) {
        String lang = getPersistedData(context, defaultLanguage);
        return setLocale(context, lang);
    }

    public static String getLanguage(Context context) {
        return getPersistedData(context, Locale.getDefault().getLanguage());
    }

    public static Context setLocale(Context context, String language) {
        persist(context, language);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return updateResources(context, language);
        }

        return updateResourcesLegacy(context, language);
    }

    public static String getPersistedData(Context context, String defaultLanguage) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(SELECTED_LANGUAGE, defaultLanguage);
    }

    private static void persist(Context context, String language) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(SELECTED_LANGUAGE, language);
        editor.apply();
    }

    @TargetApi(Build.VERSION_CODES.N)
    private static Context updateResources(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Configuration configuration = context.getResources().getConfiguration();
        configuration.setLocale(locale);
        configuration.setLayoutDirection(locale);

        Context newContect=context.createConfigurationContext(configuration);

        Resources res = newContect.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = locale;
        res.updateConfiguration(conf, dm);

        return newContect;
    }

    @SuppressWarnings("deprecation")
    private static Context updateResourcesLegacy(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Resources resources = context.getResources();

        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration.setLayoutDirection(locale);
        }
        configuration.locale = locale;

        resources.updateConfiguration(configuration, resources.getDisplayMetrics());

        return context;
    }

    public static String splitDateDDMMYY(String date){
        String subStringDateYear = date.substring(0, 4);
        String subStringDateMonth = date.substring(5, 7);
        String subStringDateDay = date.substring(8, 10);

        return subStringDateDay + "." + subStringDateMonth + "." + subStringDateYear;
    }

    public static String splitDateMMYY(String date){
        String subStringDateYear = date.substring(2, 4);
        String subStringDateMonth = date.substring(5, 7);

        return subStringDateMonth + "/" + subStringDateYear;
    }

    public static String formatDateDDMMYYYY(Date date){
        String myFormat="dd/MM/yyyy";
        SimpleDateFormat dateFormat=new SimpleDateFormat(myFormat, Locale.GERMAN);
        return dateFormat.format(date);
    }

    public static String formatDateYYYYMMDD(Date date){
        String myFormat="yyyy-MM-dd";
        SimpleDateFormat dateFormat=new SimpleDateFormat(myFormat, Locale.GERMAN);
        return dateFormat.format(date);
    }
    public static String formatJavaTimeLocalDateDDMMYYYY(java.time.LocalDate localDate){
        java.time.format.DateTimeFormatter formatter = java.time.format.DateTimeFormatter.ofPattern("dd/MM/yyyy");
        return localDate.format(formatter);
    }
    public static String formatJavaTimeLocalDateDDMMYYYY(String localDate){
        java.time.format.DateTimeFormatter formatter = java.time.format.DateTimeFormatter.ofPattern("yyyy-MM-dd");
        java.time.LocalDate date = java.time.LocalDate.parse(localDate, formatter);
        return formatJavaTimeLocalDateDDMMYYYY(date);
    }

    public static int daysBetween(String startDateString, String endDateString){
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
        LocalDate startDate = formatter.parseLocalDate(startDateString);
        LocalDate endDate = formatter.parseLocalDate(endDateString);
        return Days.daysBetween(startDate, endDate).getDays();
    }

    public static double average(int totalShit, int totalDays){
        if(totalDays == 0){
            return (double) totalShit;
        }
        Double average = (double)totalShit/(double)totalDays ;
        return round(average, 2);
    }

    private static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public static void scrollDownArrow(Activity activity, ImageView arrow, ScrollView scrollView) {
//        if(activity != null){
//            Animation animation = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.slide_down);
//            arrow.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    scrollView.fullScroll(View.FOCUS_DOWN);
//                }
//            });
//            arrow.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
//                @Override
//                public void onScrollChanged() {
//                    arrow.clearAnimation();
//                    animation.cancel();
//                    animation.reset();
//                    arrow.setVisibility(View.GONE);
//
//                    scrollView.getViewTreeObserver().removeOnScrollChangedListener(this);
//                }
//            });
//            scrollView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//                @Override
//                public void onGlobalLayout() {
//                    if (scrollView.canScrollVertically(1)) {
//                        arrow.setVisibility(View.VISIBLE);
//                        arrow.startAnimation(animation);
//                    } else {
//                        arrow.setVisibility(View.GONE);
//                    }
//                    scrollView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
//                }
//            });
//        }
    }
}